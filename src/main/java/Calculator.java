import org.apache.el.lang.ExpressionBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet(name = "Calculator", urlPatterns = "/calc")
public class Calculator extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String expressionString = req.getParameter("expression");

        Enumeration<String> parameterNames = req.getParameterNames();
        Map<String,Integer> params = new HashMap<>();

        Iterator<String> iterator = parameterNames.asIterator();
        while (iterator.hasNext()) {
            String parameterName = iterator.next();
            if ("expression".equals(parameterName)) {
                continue;
            }

            String parameterValue = req.getParameter(parameterName);
            if (params.containsKey(parameterValue)) {
                params.put(parameterName, params.get(parameterValue));
            } else {
                params.put(parameterName, Integer.parseInt(parameterValue));
            }
        }

        for (Map.Entry<String,Integer> entry: params.entrySet()) {
            expressionString = expressionString.replaceAll(entry.getKey(),Integer.toString(entry.getValue()));
        }

        ArithmeticCalculator arithmeticCalculator = new ArithmeticCalculator(expressionString,0);
        String result = arithmeticCalculator.process();

        PrintWriter printWriter = resp.getWriter();
        printWriter.print(result);
    }
}
